<?php 
include('actions/database.php');

if(isset($_GET['id']) && isset($_GET['taille'])){
            $idvoiture=$_GET['id'];
            $taille=$_GET['taille'];

            $existant= $bdd->prepare('SELECT emplacement.id, emplacement.num, emplacement.taille, emplacement.prix, occuper.id_emplacement, occuper.id_voiture, occuper.entree FROM emplacement LEFT JOIN occuper ON emplacement.id=occuper.id_emplacement WHERE occuper.id_emplacement IS NULL AND emplacement.taille=? LIMIT 1');
            $existant->execute(array($taille));//recuperer le premier emplacement vide de la meme taille que celle de la voiture

            if($existant->rowCount()==1){
                $val=$existant->fetch();
                $idempl=$val['id'];

                $insertempl=$bdd->prepare('INSERT INTO occuper(id, id_emplacement, id_voiture, entree) VALUES(?, ?, ?, now())');
                $insertempl->execute(array(NULL, $idempl, $idvoiture));

                $acceptervoiture=$bdd->prepare('UPDATE voiture set etat=? where id=?');
                $acceptervoiture->execute(array("En cours réparation", $idvoiture));

               header('Location: voirNouvVehiculeAdmin.php');

            } else{
                echo "Il n'y a aucun emplacement où vous pouvez conserver un véhicule de taille ".$taille.", veuillez en créer un...";?>
                <a href="ajoutEmplacement.php"><p>Cliquez ici pour créer un emplacement</p></a>            
            <?php } 

}else{
    echo "Veuillez sélectionner un id valide ...";
    
}

?>