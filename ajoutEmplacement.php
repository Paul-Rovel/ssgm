<!--<h1>Insersion du fichier</h1><br>
 <form method="post" action="actions/gestion/upload-fichier.php" enctype="multipart/form-data">
    <label for="mon_fichier"> Fichier (tous les formats, jusqu'à 100Mo au maximum): </label><br>
    <input type="hidden" name="MAX_FILE_SIZE" value="104857600">
    <input type="file" name="mon_fichier" id="mon_fichier"><br>
    <input type="submit" name="submit" value="Envoyer">
</form>!-->

<?php
session_start();
require('actions/gestion/ajoutEmplacementAction.php');

?>
<!DOCTYPE html>
<html lang="en">
<?php   
      include 'includes/head.php'; ?>
<body>
<?php include('includes/navbar.php') ?>

<br>
    <form class="container" method="POST">
       
       <?php 
            if(isset($errorMsg)){ echo '<p>'.$errorMsg.'<p>'; }
       ?>
    <h1>Veuillez entrer les informations du nouvel emplacement</h1><br><br>
        <div class="mb-3">
            <label for="exampleInputEmail1" class="form-label">Numéro de l'emplacement</label>
            <input type="text" class="form-control" name="num">
        </div>
        <div class="mb-3">
        <select class="form-select form-select-lg mb-3" aria-label=".form-select-lg example" name="taille">
            <option selected>Sélectionnez la taille de l'emplacement</option>
            <option value="Petit">Petit</option>
            <option value="Moyen">Moyen</option>
            <option value="Grand">Grand</option>
        </select>
        </div>
        <div class="mb-3">
            <label for="exampleInputEmail1" class="form-label">Prix de l'emplacement</label>
            <input type="text" class="form-control" name="prix">
        </div>
        <button type="submit" class="btn btn-primary" name="submit">Ajouter l'emplacement</button>
   
    </form>

    <script>src="bootstrap-4.0.0-dist/js/bootstrap.min.js" </script>
    <script>src="bootstrap-4.0.0-dist/js/jquery.js" </script>
</body>
</html>