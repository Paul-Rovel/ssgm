<?php 
require('actions/user/securiteAction.php');
require('actions/affichage/accueilAction.php');
?>
<!DOCTYPE html>
<html lang="en">
<head>
   <?php include('includes/head.php') ?>
</head>
<body style="background-color: #384454">
   <?php include('includes/navbar.php') ?>
<div class="bg-color" style="background-color: #384454">
   <div class="container"><br>
      <h4><font color="#f542b0"> Bienvenue, <?= $_SESSION['pseudo'];?>.</font></h4>
      <!-- <br>
         <div class="container">
            <form method="GET">
            <div class="form-group row">
               <div class="col-8">
                  <input type="search" name="search" class="form-control" placeholder="Rechercher" aria-label="Search">
               </div>
               <div class="col-4">
                  <button class="btn btn-success" type="submit">Rechercher</button>
               </div>
            </div>
            </form>
         </div> -->

   <br>
         <div style="display:flex; justify-content: center; margin: auto;">
            <a href="ajoutParking.php" class="btn btn-primary mx-2 p-2">Ajouter au parking</a>
            <a class="btn btn-primary mx-2 p-2" href="ajoutVoiture.php" role="button">Véhicule en panne</a>
         </div>
        
      <div style="width: 89%; display:flex; flex-wrap:wrap; justify-content: space-between; margin: auto;">
         <?php
            while($voiture=$getvoitures->fetch()){          
               ?>
               <div class="card m-3" style="width: 22rem;">
               
                     <img src="uploads/<?=$voiture['image']?>" class="card-img-top" alt="...">

                     <div class="card-body">
                        <h5 class="card-title">
                           Matricule: <?= $voiture['matricule'];?> (<?=$voiture['taille']?>)
                        </h5>
                        <?php if($voiture['id_emplacement']!=null){ ?>
                        <h5 class="card-title">
                           Emplacement N°<?= $voiture['num']; ?>
                        </h5>
                        <?php } ?>
                        <?php if($voiture['description']!=null){ ?>
                        <h6 class="card-title">
                        <p class="card-text">Informations: <?= $voiture['description']; ?></p>
                        </h6>
                        <?php } ?>
                     </div>
                     <?php if($voiture['etat']=="En attente réparation"){?>
                           <ul class="list-group list-group-flush">
                              <li class="list-group-item">Etat: En attente. Veuillez patienter un instant, votre véhicule sera examiné sous peu par l'un de nos experts.</li>
                           </ul>
                     <?php }else if($voiture['etat']=="Voiture rejettée"){?>
                           <ul class="list-group list-group-flush">
                              <li class="list-group-item">Etat: Véhicule rejetté. Votre véhicule a été rejetté, veuillez le retirer!</li>
                           </ul>
                     <?php }else if($voiture['etat']=="En cours réparation"){ ?> 
                           <ul class="list-group list-group-flush">
                              <li class="list-group-item">Etat: En cours de réparation. Votre véhicule est en cours de réparation, Veuillez patienter la fin de sa réparation.</li>
                           </ul>
                     <?php }else if($voiture['etat']=="Terminé"){ ?>
                           <ul class="list-group list-group-flush">
                              <li class="list-group-item">Etat: Terminé. La réparation de votre véhicule est terminée, et il est prêt à être récupéré! Nous vous remercions pour votre confiance.</li>
                           </ul>
                     <?php }else if($voiture['etat']=="En attente parking"){ ?> 
                           <ul class="list-group list-group-flush">
                              <li class="list-group-item">Etat: En attente pour le parking. Veuillez patienter qu'un gestionnaire accepte votre véhicule dans le parking.</li>
                           </ul>
                     <?php }else if($voiture['etat']=="En cours parking"){ ?> 
                           <ul class="list-group list-group-flush">
                              <li class="list-group-item">Etat: Dans le parking. Votre véhicule est bien gardé dans notre parking. Nous vous remercions pour votre confiance.</li>
                           </ul>
                     <?php } ?>  
                     <?php if($voiture['etat']=="En attente réparation" ){ ?>  
                           <div class="card-body" style="margin: auto;">
                              <a href="modifierVehicule.php?id=<?=$voiture['id'] ?>" class="btn btn-warning mx-2 p-2">Modifier</a>
                              <a href="supprimervoiture.php?id=<?=$voiture['id'] ?>" class="btn btn-danger mx-2 p-2">Supprimer</a>
                           </div>
                     <?php } else if($voiture['etat']=="En attente parking"){ ?>  
                           <div class="card-body" style="margin: auto;">
                              <a href="modifierVehicule2.php?id=<?=$voiture['id'] ?>" class="btn btn-warning mx-2 p-2">Modifier</a>
                              <a href="supprimervoiture.php?id=<?=$voiture['id'] ?>" class="btn btn-danger mx-2 p-2">Supprimer</a>
                           </div>
                           <?php } else if($voiture['etat']=="En cours réparation"){ ?>  
                           
                           <?php } else if($voiture['etat']=="En cours parking"){ ?>  
                           <div class="card-body">
                              <a href="supprimervoiture.php?id=<?=$voiture['id'] ?>" class="btn btn-danger">Retirer le véhicule</a>
                           </div>
                           <?php } else if($voiture['etat']=="Voiture rejettée"){ ?>  
                           <div class="card-body">
                              <a href="supprimervoiture.php?id=<?=$voiture['id'] ?>" class="btn btn-danger">Retirer le véhicule</a>
                           </div>
                           <?php } else if($voiture['etat']=="Terminé"){ ?>  
                           <div class="card-body">
                              <a href="supprimervoiture.php?id=<?=$voiture['id'] ?>" class="btn btn-danger">Retirer le véhicule</a>
                           </div>
                     <?php }?>            
                  </div>
                  <br>
               <?php
            }
         ?>
      </div>
         <br><br>
   </div>
</div>

</body>
</html>