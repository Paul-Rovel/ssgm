<nav class="navbar navbar-expand-lg navbar-dark bg-dark">
  <div class="container-fluid">
    <a class="navbar-brand" href="#">SGGM</a>
    <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarSupportedContent">
      <ul class="navbar-nav me-auto mb-2 mb-lg-0">
        <li class="nav-item">
          <?php if($_SESSION['typeCompte']=="admin"){ ?>
          <a class="nav-link active" aria-current="page" href="accueilAdmin.php">Emplacements</a>
          <?php }else{?>
            <a class="nav-link active" aria-current="page" href="accueil.php">Mes véhicules</a>
            <?php } ?>
        </li>
          <?php if($_SESSION['typeCompte']=="admin"){ ?>
            <li class="nav-item">
            <a class="nav-link active" href="voirNouvVehiculeAdmin.php">Nouveaux véhicules</a>
            </li>
            <?php }?>
        <li class="nav-item">
          <?php if($_SESSION['typeCompte']=="admin"){ ?>
            <a class="nav-link active" href="#">Règlement de facture</a>
          <?php }else{?>
            <a class="nav-link active" href="#">Règlement de facture</a>
            <?php } ?>
        </li>
        <li class="nav-item">
          <?php if($_SESSION['typeCompte']=="admin"){ ?>
            <a class="nav-link active" href="#">Grille de prix</a>
          <?php }else{?>
            <a class="nav-link active" href="#">Grille de prix</a>
            <?php } ?>
        </li>
        <li class="nav-item">
          <?php if($_SESSION['typeCompte']=="admin"){ ?>
            <a class="nav-link active" href="contact.php">Nous contacter</a>
          <?php }else{?>
            <a class="nav-link active" href="contact.php">Nous contacter</a>
            <?php } ?>
        </li>
        <li class="nav-item dropdown">
          <a class="nav-link active dropdown-toggle" href="#" id="navbarDropdown" role="button" data-bs-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><?= $_SESSION['pseudo'] ?></a>
          <ul class="dropdown-menu" aria-labelledby="navbarDropdown">
            <?php if($_SESSION['typeCompte']=="admin"){ ?>
              <li><a class="dropdown-item" href="signupAdmin.php">Créer un compte administrateur</a></li>
            <?php }?>
            <li><a class="dropdown-item" href="actions/user/logoutAction.php">Déconnexion</a></li>
          </ul>
        </li>
      </ul>
    </div>
  </div>
</nav>

<script src="js/jquery-3.3.1.slim.min.js"></script>
<script src="js/bootstrap.bundle.min.js"></script>