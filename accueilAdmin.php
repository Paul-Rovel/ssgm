<?php 
require('actions/user/securiteAction.php');
require('actions/affichage/accueilActionAdmin.php');
?>
<!DOCTYPE html>
<html lang="en">
<head>
   <?php include('includes/head.php') ?>
</head>
<body style="background-color: #384454">
   <?php include('includes/navbar.php') ?>
<div class="bg-color" style="background-color: #384454">
   <div class="container"><br>
      <h4><font color="#f542b0"> Bienvenue, <?= $_SESSION['pseudo'];?>.</font></h4>
      <!-- <br>
         <div class="container">
            <form method="GET">
            <div class="form-group row">
               <div class="col-8">
                  <input type="search" name="search" class="form-control" placeholder="Rechercher" aria-label="Search">
               </div>
               <div class="col-4">
                  <button class="btn btn-success" type="submit">Rechercher</button>
               </div>
            </div>
            </form>
         </div> -->

      <br>
            <div style="display:flex; justify-content: center; margin: auto;">
               <a href="ajoutEmplacement.php" class="btn btn-primary">Ajouter un emplacement</a>
            </div>
            <br>
      <div style="width: 89%; display:flex; flex-wrap:wrap; justify-content: space-between; margin: auto;">
         <?php
            while($empl=$getempl->fetch()){            
               ?>
               
               <div class="card m-3" style="width: 22rem;">
                        <h5 class="card-title">
                           Emplacement N°<?= $empl['num']; ?> (<?= $empl['taille']; ?>)
                        </h5>
                     <?php if($empl['image']!=null && $empl['matricule']!=null){ ?>
                     <img src="uploads/<?=$empl['image']?>" class="card-img-top" alt="...">
                     <?php }else{?>
                     <img src="uploads/emplacement" class="card-img-top" alt="..."> 
                     <?php }
                     ?>
                     <?php if($empl['image']!=null){ ?>
                     <div class="card-body">
                        <?php if($empl['matricule']!=null){ ?>
                        <h5 class="card-title">
                           Matricule: <?= $empl['matricule'];?> (<?=$empl['taille']?>)
                        </h5>
                        <?php }?>
                        <?php if($empl['description']!=null){ ?>
                        <h6 class="card-title">
                        <p class="card-text">Informations: <?= $empl['description']; ?></p>
                        </h6>
                        <?php } ?>                  
                     </div>
                     <?php } ?>
                     <?php if($empl['etat']=="En attente réparation"){?>
                           <ul class="list-group list-group-flush">
                              <li class="list-group-item">Etat: En attente. Le propriétaire du véhicule est en attente de votre confirmation pour réparation de son véhicule.</li>
                           </ul>
                     <?php }else if($empl['etat']=="En attente parking"){?>
                           <ul class="list-group list-group-flush">
                              <li class="list-group-item">Etat: En attente. Le propriétaire du véhicule est en attente de votre confirmation pour être accepté dans le parking.</li>
                           </ul>
                     <?php }else if($empl['etat']=="En cours réparation"){ ?> 
                           <ul class="list-group list-group-flush">
                              <li class="list-group-item">Etat: En cours. Un réparateur se charge actuellement du véhicule.</li>
                           </ul>
                     <?php }else if($empl['etat']=="Terminé"){ ?>
                           <ul class="list-group list-group-flush">
                              <li class="list-group-item">Etat: Terminé. La réparation de ce véhicule est terminée.</li>
                           </ul>
                     <?php }else if($empl['etat']=="En cours parking"){ ?>
                           <ul class="list-group list-group-flush">
                              <li class="list-group-item">Etat: Dans le parking. Le véhicule est garé dans l'emplacement.</li>
                           </ul>
                     <?php }else if($empl['etat']=="Voiture rejettée"){ ?>
                           <ul class="list-group list-group-flush">
                              <li class="list-group-item">Etat: Rejetté. Le véhicule a été rejetté par le garage.</li>
                           </ul>
                     <?php } ?>  
                           <?php if($empl['image']==null){ ?>
                           <div class="card-body">
                              <a href="supprimeremplacement.php?id=<?=$empl['id'] ?>" class="btn btn-danger">Retirer emplacement</a>
                           </div>
                           <?php }else if($empl['etat']=="En cours réparation"){?>
                           <div class="card-body">
                              <a href="finreparation.php?id=<?=$empl['id_voiture'] ?>" class="btn btn-success">Réparation terminée</a>
                           </div> 
                           <?php }else if($empl['etat']=="En cours parking"){?>
                           <div class="card-body">
                              <a href="proprietaire.php?id=<?=$empl['id_proprio'] ?>" class="btn btn-success">Voir le propriétaire</a>
                           </div> 
                           <?php }else if($empl['etat']=="Terminé"){?>
                           <div class="card-body">
                              <a href="proprietaire.php?id=<?=$empl['id_proprio'] ?>" class="btn btn-success">Voir propriétaire</a>
                              <a href="#" class="btn btn-danger">Ejecter véhicule</a>
                           </div>  
                           <?php } ?>  
                  </div>
                     <br>
               <?php
            }
         ?>
        
      </div>
      <br><br>
   </div>
</div>
</body>
</html>