# SSGM

SSGM est une application de gestion de garage modern, développée en PHP et Bootstrap.

## Installation

Vous avez juste à telecharger le fichier zip et à l'extraire dans le dossier www de wampserver, et importer la Base de données de nom garage.


## Comment utiliser

Vérifier que vos services de wamps soient bien démarrés,
Connectez vous au serveur a partir dun autre dispositif (tel, pc...), en entrant adresse_du_serveur/ssgm,
Vous créez un compte sur la page d'inscription ou vous vous connectez, ensuite, si vous etes admin vous pouvez accepter des vehicules (reparation, parking), demarrer la reparation, terminer...
Et en tant qutilisateur vous pouvez enregistrer votre vehicule pour parking ou pour reparation.
Le paiement sera inclus dans l'application prochainement...

## License

[MIT](https://choosealicense.com/licenses/mit/)