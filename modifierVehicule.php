<?php 
require('actions/user/securiteAction.php');
require('actions/gestion/recupinfoVehiculeAction.php');
require('actions/gestion/modifierVehiculeAction.php');
?>

<DOCTYPE html>
<html lang="en">
<head>
 <?php include('includes/head.php');?>
</head>
<body>
    <?php include('includes/navbar.php');?>
    <br><br>

    <div class="container">
        <?php include('errorsuccessmsg.php');?>

        <?php
        if(isset($voiture_img)){?>
            <form class="container" method="POST" enctype="multipart/form-data">
            <h1>Veuillez entrer les nouvelles informations de votre véhicule</h1><br><br>
            <div class="mb-3">
            <label for="exampleInputEmail1" class="form-label">Image du véhicule</label>
            <input type="hidden" name="MAX_FILE_SIZE" value="104857600">
            <input type="file" class="form-control" name="mon_fichier" id="mon_fichier">
        </div>
            <div class="mb-3">
            <label for="exampleInputEmail1" class="form-label">Matricule</label>
            <input type="text" class="form-control" name="matricule" value="<?=$voiture_matricule?>">
        </div>
        <div class="mb-3">
        <select class="form-select form-select-lg mb-3" aria-label=".form-select-lg example" name="taille">
            <option selected><?=$voiture_taille?></option>
            <option value="Petit">Petit</option>
            <option value="Moyen">Moyen</option>
            <option value="Grand">Grand</option>
        </select>
        </div>
        <div class="mb-3">
            <label for="exampleInputEmail1" class="form-label">Description du problème du véhicule</label>
            <textarea type="text" class="form-control" name="desc_probleme"><?=$voiture_desc?></textarea>
        </div>

            <button type="submit" class="btn btn-primary" name="modifiervoiture">Modifier le véhicule</button>

            </form> 
        <?php } ?>

    </div>
    

</body>
</html>