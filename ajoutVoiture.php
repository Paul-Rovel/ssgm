<!--<h1>Insersion du fichier</h1><br>
 <form method="post" action="actions/gestion/upload-fichier.php" enctype="multipart/form-data">
    <label for="mon_fichier"> Fichier (tous les formats, jusqu'à 100Mo au maximum): </label><br>
    <input type="hidden" name="MAX_FILE_SIZE" value="104857600">
    <input type="file" name="mon_fichier" id="mon_fichier"><br>
    <input type="submit" name="submit" value="Envoyer">
</form>!-->

<?php
session_start();
require('actions/gestion/upload-fichier.php');
?>
<!DOCTYPE html>
<html lang="en">
<?php   
      include 'includes/head.php'; ?>
<body>
<?php include('includes/navbar.php') ?>

<br>
    <form class="container" method="POST" enctype="multipart/form-data">
       
       <?php 
            if(isset($errorMsg)){ echo '<p>'.$errorMsg.'<p>'; }
       ?>
    <h1>Veuillez entrer les informations de votre véhicule</h1><br><br>
        <div class="mb-3">
            <label for="exampleInputEmail1" class="form-label">Image du véhicule (10Mo au maximum)</label>
            <input type="hidden" name="MAX_FILE_SIZE" value="10485760">
            <input type="file" class="form-control" name="mon_fichier" id="mon_fichier">
        </div>
        <div class="mb-3">
            <label for="exampleInputEmail1" class="form-label">Matricule</label>
            <input type="text" class="form-control" name="matricule">
        </div>
        <div class="mb-3">
        <select class="form-select form-select-lg mb-3" aria-label=".form-select-lg example" name="taille">
            <option selected>Sélectionnez la taille de votre véhicule</option>
            <option value="Petit">Petit</option>
            <option value="Moyen">Moyen</option>
            <option value="Grand">Grand</option>
        </select>
        </div>
        <div class="mb-3">
            <label for="exampleInputEmail1" class="form-label">Description du problème du véhicule</label>
            <textarea type="text" class="form-control" name="desc_probleme"></textarea>
        </div>

        <button type="submit" class="btn btn-primary" name="submit">Ajouter le véhicule</button>
   
    </form>

    <script>src="bootstrap-4.0.0-dist/js/bootstrap.min.js" </script>
    <script>src="bootstrap-4.0.0-dist/js/jquery.js" </script>
</body>
</html>