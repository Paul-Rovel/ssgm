-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Hôte : 127.0.0.1:3306
-- Généré le : Dim 25 juin 2023 à 06:33
-- Version du serveur :  5.7.31
-- Version de PHP : 7.3.21

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données : `garage`
--
CREATE DATABASE IF NOT EXISTS `garage` DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci;
USE `garage`;

-- --------------------------------------------------------

--
-- Structure de la table `emplacement`
--

DROP TABLE IF EXISTS `emplacement`;
CREATE TABLE IF NOT EXISTS `emplacement` (
  `id` int(255) NOT NULL AUTO_INCREMENT,
  `num` int(255) NOT NULL,
  `taille` varchar(255) NOT NULL,
  `prix` int(255) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `num` (`num`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `emplacement`
--

INSERT INTO `emplacement` (`id`, `num`, `taille`, `prix`) VALUES
(1, 1, 'Moyen', 5000),
(3, 2, 'Petit', 2500),
(4, 3, 'Grand', 10000),
(5, 4, 'Grand', 10000);

-- --------------------------------------------------------

--
-- Structure de la table `occuper`
--

DROP TABLE IF EXISTS `occuper`;
CREATE TABLE IF NOT EXISTS `occuper` (
  `id` int(255) NOT NULL AUTO_INCREMENT,
  `id_emplacement` int(255) NOT NULL,
  `id_voiture` int(255) NOT NULL,
  `entree` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `occuper_ibfk_2` (`id_emplacement`),
  KEY `occuper_ibfk_3` (`id_voiture`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `occuper`
--

INSERT INTO `occuper` (`id`, `id_emplacement`, `id_voiture`, `entree`) VALUES
(5, 1, 7, '2023-04-26 03:36:53'),
(6, 3, 13, '2023-05-05 10:10:54'),
(7, 4, 16, '2023-05-05 10:22:10');

-- --------------------------------------------------------

--
-- Structure de la table `users`
--

DROP TABLE IF EXISTS `users`;
CREATE TABLE IF NOT EXISTS `users` (
  `id` int(255) NOT NULL AUTO_INCREMENT,
  `pseudo` varchar(255) NOT NULL,
  `nom` varchar(255) NOT NULL,
  `prenom` varchar(255) NOT NULL,
  `tel` bigint(255) NOT NULL,
  `mdp` varchar(255) NOT NULL,
  `typeCompte` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `users`
--

INSERT INTO `users` (`id`, `pseudo`, `nom`, `prenom`, `tel`, `mdp`, `typeCompte`) VALUES
(1, 'ShadowX', 'Fokam Kamga', 'Paul Rovel', 650239073, '$2y$10$f8pHAavH.N7jZlJ0eWRJw.Ons3UiqEe/QoOpG.0QFP58hDdJYFVb6', 'admin'),
(2, 'Maffo', 'Maffo', 'Michel', 665858966, '$2y$10$9NltvasgaCXEgtEpyn4obOnSxp0.poRrBzEmfzj4w4WmH5OrQGfQ.', 'user'),
(3, 'Willy', 'Ella', 'Tiam', 664527854, '$2y$10$3ZSlO8lWXErm731Yk3fSv.En61X4es25f0u2PDUIkbK1msNs7MzUO', 'admin'),
(4, 'Gust', 'Ella', 'Willy', 664668, '$2y$10$rZdIT0TM/.kr2JL0gufR3eyrHt/lrwz2CVpHt/.Jki8sWb6QUMLzq', 'user'),
(5, 'M Membou', 'Membou', 'Wilson', 566546, '$2y$10$YY/sQGhx.2LI7Y.eCQanne/3heERlvsQSeFp5XHWkayXBH/SATqWu', 'user'),
(6, 'Mbesse', 'Mbesse', 'Ulrich ', 659133336, '$2y$10$jVMIEyk5LDqJqOZh.gv5a.XNlXYC0mpgG5m/h691CiVQ9jPoucoq2', 'user');

-- --------------------------------------------------------

--
-- Structure de la table `voiture`
--

DROP TABLE IF EXISTS `voiture`;
CREATE TABLE IF NOT EXISTS `voiture` (
  `id` int(255) NOT NULL AUTO_INCREMENT,
  `id_proprio` int(255) NOT NULL,
  `matricule` varchar(255) NOT NULL,
  `image` varchar(255) NOT NULL,
  `description` varchar(255) NOT NULL,
  `taille` varchar(255) NOT NULL,
  `etat` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `voiture`
--

INSERT INTO `voiture` (`id`, `id_proprio`, `matricule`, `image`, `description`, `taille`, `etat`) VALUES
(7, 2, 'ZTRBSRY7S', 'voiture1.jpg', '', 'Moyen', 'En cours parking'),
(12, 2, 'QZNJQ56E', 'voiture.jpg', '', 'Moyen', 'En attente parking'),
(13, 2, 'VQKSNLD45', 'moto2.jpg', '', 'Petit', 'En cours parking'),
(14, 2, 'SYNNRQ45', 'voiture10.jpg', '', 'Moyen', 'En attente parking'),
(15, 2, 'TVEBTDYNYJ', 'Bus1.jpg', '', 'Grand', 'En attente parking'),
(16, 5, 'bstrgbrrsybh', 'camion1.jpg', 'Les vitesses n\'entr', 'Grand', 'En cours réparation');

--
-- Contraintes pour les tables déchargées
--

--
-- Contraintes pour la table `occuper`
--
ALTER TABLE `occuper`
  ADD CONSTRAINT `occuper_ibfk_2` FOREIGN KEY (`id_emplacement`) REFERENCES `emplacement` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `occuper_ibfk_3` FOREIGN KEY (`id_voiture`) REFERENCES `voiture` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
