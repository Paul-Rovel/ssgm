<?php 
require('actions/user/securiteAction.php');
require('actions/affichage/voirNouvVehiculeActionAdmin.php');
?>
<!DOCTYPE html>
<html lang="en">
<head>
   <?php include('includes/head.php') ?>
</head>
<body style="background-color: #384454">
   <?php include('includes/navbar.php') ?>
<div class="bg-color" style="background-color: #384454">
   <div class="container">
      <?php include('errorsuccessmsg.php'); ?><br>

         <h4><font color="#f542b0"> Bienvenue, <?= $_SESSION['pseudo'];?>.</font></h4>
         <!-- <br>
            <div class="container">
               <form method="GET">
               <div class="form-group row">
                  <div class="col-8">
                     <input type="search" name="search" class="form-control" placeholder="Rechercher" aria-label="Search">
                  </div>
                  <div class="col-4">
                     <button class="btn btn-success" type="submit">Rechercher</button>
                  </div>
               </div>
               </form>
            </div> 
            <br><br>-->
            <br>
      <div style="width: 89%; display:flex; flex-wrap:wrap; justify-content: space-between; margin: auto;">

         <?php
            while($voiture=$getvoitures->fetch()){            
               ?>
               <br><br>
               <div class="card m-3" style="width: 22rem;">
               
                     <img src="uploads/<?=$voiture['image']?>" class="card-img-top" alt="...">

                     <div class="card-body">
                        <h5 class="card-title">
                           Matricule: <?= $voiture['matricule']; ?> (<?=$voiture['taille']?>)
                        </h5>
                        <?php if($voiture['id_emplacement']!=null){ ?>
                        <h5 class="card-title">
                           Emplacement N°<?= $voiture['num']; ?>
                        </h5>
                        <?php } ?>
                        <?php if($voiture['description']!=null){ ?>
                        <h6 class="card-title">
                        <p class="card-text">Informations: <?= $voiture['description']; ?></p>
                        </h6>
                        <?php } ?>
                     </div>
                     <?php if($voiture['etat']=="En attente réparation"){?>
                           <ul class="list-group list-group-flush">
                              <li class="list-group-item">Etat: En attente. Le propriétaire du véhicule est en attente de votre confirmation pour réparation de son véhicule.</li>
                           </ul>
                     <?php }else if($voiture['etat']=="En attente parking"){?>
                           <ul class="list-group list-group-flush">
                              <li class="list-group-item">Etat: En attente. Le propriétaire du véhicule est en attente de votre confirmation pour être accepté dans le parking.</li>
                           </ul>
                     <?php }else if($voiture['etat']=="En cours réparation"){ ?> 
                           <ul class="list-group list-group-flush">
                              <li class="list-group-item">Etat: En cours. Un réparateur se charge actuellement du véhicule.</li>
                           </ul>
                     <?php }else if($voiture['etat']=="Terminé"){ ?>
                           <ul class="list-group list-group-flush">
                              <li class="list-group-item">Etat: Terminé. La réparation de ce véhicule est terminée.</li>
                           </ul>
                     <?php }else if($voiture['etat']=="En cours parking"){ ?>
                           <ul class="list-group list-group-flush">
                              <li class="list-group-item">Etat: Dans le parking. Le véhicule est garé dans l'emplacement.</li>
                           </ul>
                     <?php }else if($voiture['etat']=="Voiture rejettée"){ ?>
                           <ul class="list-group list-group-flush">
                              <li class="list-group-item">Etat: Rejetté. Le véhicule a été rejetté par le garage.</li>
                           </ul>
                     <?php }else { ?>
                           <ul class="list-group list-group-flush">
                              <li class="list-group-item">Etat: Pas normal.</li>
                           </ul>
                     <?php } ?> 
                           <div class="card-body">
                              <?php if($voiture['etat']=="En attente parking"){ ?>
                                 <a href="acceptervoitureparking.php?id=<?=$voiture['id']?>&taille=<?=$voiture['taille']?>" class="btn btn-primary">Accepter véhicule</a>
                              <?php }else if($voiture['etat']=="En attente réparation"){?>
                                 <a href="acceptervoiture.php?id=<?=$voiture['id']?>&taille=<?=$voiture['taille']?>" class="btn btn-primary">Accepter véhicule</a>
                              <?php }else {?>
                                 <a href="#" class="btn btn-primary">Pas normal</a>
                              <?php }?>
                              <a href="rejettervoiture.php?id=<?=$voiture['id']?>" class="btn btn-danger">Rejetter véhicule</a>
                           </div>
                     </div>
               <?php
            }
         ?>
      </div>
            <br><br>
   </div>
   
</div>

</body>
</html>